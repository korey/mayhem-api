// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate')

var schema = new mongoose.Schema(
  {
    date : Number,
    title : String,
    body : String,
    author : String,
    tags : [String]
},{
   timestamps: true
  })

schema.plugin(mongoosePaginate)

module.exports = mongoose.model('blog', schema)


/*
// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('blog', new Schema({
    date : Number,
    title : String,
    body : String,
    author : String,
    tags : [String]
},{
   timestamps: true
  }
));
*/
