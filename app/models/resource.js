// get an instance of mongoose and mongoose.Schema
let mongoose = require('mongoose')
let Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')


let schema = new Schema(
  {
    name: String,
    type: {type: Schema.Types.ObjectId, ref: 'ResourceType'},
    description: String,
    dependencies: [{type: Schema.Types.ObjectId, ref: 'Resource'}],
    RTO: {type: Schema.Types.ObjectId, ref: 'ResourceRTOValue'},
    RPO: String,
    notes: String,
    tags: [{type: Schema.Types.ObjectId, ref: 'Tag'}],
    relatedVenders: [{type: Schema.Types.ObjectId, ref: 'Vender'}],
    DRTestRequired: Boolean
  },
  {
     timestamps: true
  }

)

schema.plugin(mongoosePaginate)
module.exports = mongoose.model('Resource', schema)
