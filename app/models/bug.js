// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('bug', new Schema({
    openDate : Number,
    type : String,
    description : String,
    notes : String,
    submittedBy : String,
    priority: String,
    closed: Boolean,
    closedBy: String,
    closedDate: Number
},{
   timestamps: true
  }
));
