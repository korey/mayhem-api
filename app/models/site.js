// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Site', new Schema({
  name: String,
  siteType: Schema.Types.ObjectId,    //_Id of Location Type
  description: String,
  addressStreet1: String,
  addressStreet2: String,
  addressCity: String,
  addressState: String,
  addressZip: String,
  generatorBackup: Boolean,
  ATMITM: [Schema.Types.ObjectId]
  },
  {
   timestamps: true
  }
));

