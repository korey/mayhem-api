// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// set up a mongoose model and pass it using module.exports

module.exports = mongoose.model('Department', new Schema({
    name: String,
    parent: { type: Schema.Types.ObjectId,  ref: 'Department' },   //_Id of Parent Department
    manager: { type: Schema.Types.ObjectId, ref: 'User' },   //employeeId from personel
    BCPPlanOwner: { type: Schema.Types.ObjectId, ref: 'User' }, //employeeId from personel
    BCPSeatRequired: Number,
    RemoteAccessRequired: Number,
    simulationTestReq: Boolean,
    primarySite: [{type: Schema.Types.ObjectId, ref: 'Site'}],
    primaryLocation: String,
    backupSite: [{type: Schema.Types.ObjectId, ref: 'Site'}],
    backupLocation: String,
    description: String,
    division: Schema.Types.ObjectId,
    resourcesRequired: [{type: Schema.Types.ObjectId, ref: 'Resource'}]
  },
  {
   timestamps: true
  }
));
