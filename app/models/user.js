// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')

let schema = new mongoose.Schema(
  {
    fname: String,
    lname: String,
    mIntial: String,
    title: String,
    department: { type: Schema.Types.ObjectId,  ref: 'Department' },
    emergencyCallFrom: String,
    officePhone: String,
    officePhoneExt: String,
    homePhone: String,
    mobilePhone: String,
    email: String,
    tags: [Schema.Types.ObjectId],
    loginAllowed: { type: Boolean, default: false },
    password: String,
    admin: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true }
  },
  {
   timestamps: true
 })

schema.plugin(mongoosePaginate)

module.exports = mongoose.model('User', schema)
