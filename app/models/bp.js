// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
mongoose = require('mongoose').set('debug', true);
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('bp', new Schema({
  name: String,
  department: [{type: Schema.Types.ObjectId, ref: 'Department'}],
  description: String,
  riskReputation: String,
  riskLegal: String,
  riskOperational: String,
  riskFinancial: String,
  frequency: String,
  frequencyNotes: String,
  maxAllowedDowntime: String,
  requiredResources: [{type: Schema.Types.ObjectId, ref: 'Resource'}]
  },
  {
   timestamps: true
  }
));
