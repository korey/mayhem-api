// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate')

let schema = new mongoose.Schema(
  {
    name: String,
    description: String,
    accountNumber: String,
    addressStreet1: String,
    addressStreet2: String,
    addressCity: String,
    addressState: String,
    addressZip: String,
    website: String,
    phone: String,
    type: String, // Reseller, Manufacturer, Supplier, Manufacturer, Service Provider
    risk: String, // Critical, Key, Normal
    contacts: [{ type: Schema.Types.ObjectId,  ref: 'Contact' }]
  },
  {
    timestamps: true
  }
)

schema.plugin(mongoosePaginate)

module.exports = mongoose.model('Vender', schema)
