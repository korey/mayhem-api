// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Contact', new Schema({
    fname: String,
    lname: String,
    vender: { type: Schema.Types.ObjectId,  ref: 'Vender' },
    title: String,
    officePhone: String,
    officePhoneExt: String,
    mobilePhone: String,
    fax: String,
    email: String,
  },
  {
   timestamps: true
  }
));
