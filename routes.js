// API ROUTES
// =======================
const express = require('express')
const router = express.Router() //get instance of express router
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt') // www.npmjs.com/package/bcrypt
const User = require('./app/models/user')
const Department = require('./app/models/department')
const Site = require('./app/models/site')
const Vender = require('./app/models/vender')
const Resource = require('./app/models/resource')
const ResourceType = require('./app/models/resourceType')
const ResourceRTOValue = require('./app/models/resourceRTOValue')
const Contact = require('./app/models/contact')
const Bp = require('./app/models/bp.js')
const Blog = require('./app/models/blog.js')
const Bug = require('./app/models/bug.js')

const config = require('./config')

//bcrpty settings
const saltRounds = 10


// route to authenticate a user (POST http://localhost:9090/api/auth)
router.post('/auth', function (req, res) {

  // find the user
  User.findOne({
    email: req.body.email
  }, function (err, user) {

    if (err) throw err

    if (!user) {
      console.log('No User Found')
      res.status(401).json({
        success: false,
        message: 'Authentication failed. User not found.',
        error: 'Authentication failed. User not found.'
      });

    } else if (user) {

      // check if password matches
      /*
      // Load hash from your password DB. 
      bcrypt.compare(myPlaintextPassword, hash, function(err, res) {
          // res == true or false
      })
      */
      bcrypt.compare(req.body.password, user.password, function (err, compareResult) {
        if (!compareResult) {//password failed
          console.log('Found user but bad password')
          res.status(401).json({
            success: false,
            message: 'Authentication failed. Wrong password.',
            error: 'Authentication failed. Wrong password.'
          })
        } else {
          //password is good
          // create a token
          console.log('Success, User authenticated')
          let token = jwt.sign(user, config.secret, {
            expiresIn: 21600000 // expires in 6 hours in seconds
          })

          // return the information including token as JSON
          res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
          })

        }

      })

    }

  })
})


//route middleware to verify a token
router.use(function (req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, config.secret, function (err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: 'Failed to authenticate token.'
        });
      } else {
        // if everything is good, save to request for use in other routes
        res.locals.user = decoded._doc
        //console.log(`Request Decoded: ${JSON.req.body.ify(req.decoded._doc)}`)
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
});

//ALL ROUTES BELOW MUST BE AUTHENTICATED///////////
//////////////////
////////////////
///////////////
////////////
///////////
/////////
///////
/////
///



// route to show a random message (GET http://localhost:9090/api/)
router.get('/', function (req, res) {
  res.json({ message: 'Welcome to the Mayhem API...' });
});

//////// USER ROUTES /////////////
//////////////////////////////////

// route to return all personel (GET http://localhost:9090/api/users)
router.get('/users', function (req, res) {
  User.find({}, '_id fname lname', function (err, personel) {
    res.json(personel);
  });
});

//route returns all users paginated
router.get('/users/page/:page', function (req, res) {

  let page = req.params.page
  let limit = 15

  User.paginate({}, { select: 'fname lname title isActive', sort: { lname: 1 }, page: page, limit: limit }, function (err, personel) {
    res.json(personel)
  })

})


//route returns all users paginated by Letter of last name
router.get('/users/letter/:letter/:page', function (req, res) {

  let page = req.params.page
  let limit = 15
  let letter = req.params.letter.toLowerCase()
  let searchRegEx = `^${letter}`

  User.paginate({ "lname": { $regex: searchRegEx, $options: "i" } }, { select: 'fname lname title isActive', sort: { lname: 1 }, page: page, limit: limit }, function (err, personel) {
    res.json(personel)
  })

})

//Route adds a new user
router.post('/users', function (req, res) {
  bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
    req.body.password = hash

    let newUser = new User(req.body)
    newUser.save(function (err, newUser) {
      if (err) throw err
      console.log(`New user Added: ${newUser.fname}`)
      res.json(newUser)
    })

  })


})

// route to return one user (GET http://localhost:9090/api/user)
router.get('/users/:userId', function (req, res) {
  let searchId = ""
  if (req.params.userId == 'me') {
    searchId = res.locals.user._id
    console.log(`'Me' is the user: ${searchId}`)
  } else {
    searchId = req.params.userId
    console.log(`Serching for the user: ${searchId}`)
  }
  User.findById(searchId)
    .populate('department')
    .then(function (user) {
      res.json(user)
    })

});


//route to update a user
router.put('/users/:userId', function (req, res) {
  if (req.body.updatePassword == false) {
    delete req.body['password']
    //password reset NOT requested saving user
    console.log('password reset NOT requested... saving user')
    if (req.body.department == "") {
      delete req.body['department']
    }
    User.update({ _id: req.body._id }, {
      $set: {
        fname: req.body.fname,
        lname: req.body.lname,
        mIntial: req.body.mIntial,
        title: req.body.title,
        department: req.body.department,
        emergencyCallFrom: req.body.emergencyCallFrom,
        officePhone: req.body.officePhone,
        officePhoneExt: req.body.officePhoneExt,
        homePhone: req.body.homePhone,
        mobilePhone: req.body.mobilePhone,
        email: req.body.email,
        tags: req.body.tags,
        loginAllowed: req.body.loginAllowed,
        admin: req.body.admin,
        isActive: req.body.isActive
      }
    }, function (err, user) {
      if (err) throw err
      res.json(user)
    })

  } else {
    //password reset has been requested
    console.log("Hashing/Salting password...cuurent password: " + req.body.password)
    bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
      if (err) throw err
      console.log(`Hashing complete.... new password: ${hash}`)
      console.log("updating user/personell")
      if (req.body.department == "") {
        delete req.body['department']
      }
      User.update({ _id: req.body._id }, {
        $set: {
          fname: req.body.fname,
          lname: req.body.lname,
          mIntial: req.body.mIntial,
          title: req.body.title,
          department: req.body.department,
          emergencyCallFrom: req.body.emergencyCallFrom,
          officePhone: req.body.officePhone,
          officePhoneExt: req.body.officePhoneExt,
          homePhone: req.body.homePhone,
          mobilePhone: req.body.mobilePhone,
          email: req.body.email,
          tags: req.body.tags,
          loginAllowed: req.body.loginAllowed,
          password: hash,
          admin: req.body.admin,
          isActive: req.body.isActive
        }
      }, function (err, user) {
        if (err) throw err
        res.json(user)
      })

    })
  }





})

//Route deletes a user by sent Id
router.delete('/users/:userId', function (req, res) {

  User.remove({ _id: req.params.userId }, function (err) {
    if (err) throw err
    res.json({ status: "ok", msg: "User deleted" })
  })

})


//////// Site ROUTES /////////////
//////////////////////////////////

// route to return Sites 
router.get('/sites', function (req, res) {
  Site.find({}, function (err, sites) {
    res.json(sites);
  });
});

//////// Department ROUTES ///////
//////////////////////////////////

//route returns all departments (GET http://localhost:9090/api/departments)
router.get('/departments', function (req, res) {

  Department.find({})
    .populate('parent')
    .then(function (departments) {
      //sort departments alphabetically

      departments.sort(function (a, b) {
        let textA = a.name.toLowerCase()
        let textB = b.name.toLowerCase()
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0
      })
      return departments
    })
    .then(function (departments) {
      res.json(departments)
    })

})


//route returns department of given id (GET http://localhost:9090/api/department)
router.get('/department/:deptId', function (req, res) {

  Department.findById(req.params.deptId)
    .populate('manager parent primarySite backupSite resource BCPPlanOwner resourcesRequired')
    .then(function (department) {
      res.json(department)
    })

})

//route to update a department (PUT http://localhost:9090/api/department/:deptId)
router.put('/department/:deptId', function (req, res) {
  console.log("updating department")
  Department.findByIdAndUpdate(req.params.deptId, req.body, function (err, dept) {
    if (err) throw err
    res.json(dept)
  })
})

//route returns the business processes for a department of given id
router.get('/department/bprocess/:deptId', function (req, res) {

  Bp.find({ department: req.params.deptId })
    .then(function (bps) {
      res.json(bps)
    })

})

//route removes the business processes form the department of given id
router.put('/department/removebp/:bpId/:deptId', function (req, res) {

  Bp.findById(req.params.bpId, function (err, bp) {
    let index = bp.department.indexOf(req.params.deptId)
    bp.department.splice(index, 1)

    Bp.findByIdAndUpdate(req.params.bpId, { $set: { department: bp.department } }, function (err, data) {
      if (err) throw err
      res.json({
        status: "ok",
        msg: "Bp removed from department.",
        data: data
      })
    })
  })

})

////////Department Personel ROUTES////////
//////////////////////////////////////////


// route to return all dept personel 
router.get('/deptPersonel/:deptId', function (req, res) {
  let deptId = req.params.deptId

  User.find({department: deptId}, '_id fname lname', function (err, deptPersonel) {
    res.json(deptPersonel);
  });
});

//route returns all dept users paginated
router.get('/deptPersonel/page/:page/:deptId', function (req, res) {

  let deptId = req.params.deptId
  let page = req.params.page
  let limit = 15

  User.paginate({department : deptId}, { select: 'fname lname title isActive', sort: { lname: 1 }, page: page, limit: limit }, function (err, personel) {
    res.json(personel)
  })

})


//route returns all dept users paginated by Letter of last name
router.get('/deptPersonel/letter/:letter/:page/:deptId', function (req, res) {

  let deptId = req.params.deptId
  let page = req.params.page
  let limit = 15
  let letter = req.params.letter.toLowerCase()
  let searchRegEx = `^${letter}`

  User.paginate({ department: deptId, "lname": { $regex: searchRegEx, $options: "i" } }, { select: 'fname lname title isActive', sort: { lname: 1 }, page: page, limit: limit }, function (err, personel) {
    res.json(personel)
  })

})




//////// Vender ROUTES ///////////
//////////////////////////////////

//route returns all venders paginated (GET http://localhost:9090/api/venders)
router.get('/venders/page/:page', function (req, res) {

  let page = req.params.page
  let limit = 15

  Vender.paginate({}, { select: 'name description', sort: { name: 1 }, page: page, limit: limit }, function (err, venders) {
    res.json(venders)
  })

})


//route returns all venders paginated by Letter
router.get('/venders/letter/:letter/:page', function (req, res) {

  let page = req.params.page
  let limit = 15
  let letter = req.params.letter.toLowerCase()
  let searchRegEx = `^${letter}`

  Vender.paginate({ "name": { $regex: searchRegEx, $options: "i" } }, { select: 'name description', sort: { name: 1 }, page: page, limit: limit }, function (err, venders) {
    res.json(venders)
  })

})

router.get('/venders/all', function (req, res) {

  Vender.find({}, 'name description')
    .then(function (venders) {
      res.json(venders)
    })
})


//route returns vender of given id
router.get('/vender/:venderId', function (req, res) {

  Vender.findById(req.params.venderId)
    .populate('contacts')
    .then(function (vender) {
      res.json(vender)
    })

})

//route to update a vender
router.put('/vender', function (req, res) {
  console.log('updating vender...')

  Vender.update({ _id: req.body._id }, {
    $set: {
      name: req.body.name,
      description: req.body.description,
      accountNumber: req.body.accountNumber,
      addressStreet1: req.body.addressStreet1,
      addressStreet2: req.body.addressStreet2,
      addressCity: req.body.addressCity,
      addressState: req.body.addressState,
      addressZip: req.body.addressZip,
      website: req.body.website,
      phone: req.body.phone,
      type: req.body.type,
      risk: req.body.risk,
      contacts: req.body.contacts
    }
  }, function (err, ven) {
    if (err) throw err
    res.json(ven)
  })

})


//Route adds a new vender (POST http://localhost:9090/api/vender)
router.post('/vender', function (req, res) {
  let newUser = new Vender(req.body)
  newUser.save(function (err, newUser) {
    if (err) throw err
    console.log(`New Vender Added: ${newUser.name}`)
    res.json(newUser)
  })

})

//Route deletes a vender by sent Id
router.delete('/vender/:venderId', function (req, res) {
  Vender.remove({ _id: req.params.venderId }, function (err) {
    if (err) throw err
    res.json({ status: "ok", msg: "Vender deleted" })
  })
})

//////// Resource ROUTES ///////////
//////////////////////////////////

//route returns all resources paginated
router.get('/resources/page/:page', function (req, res) {

  let page = req.params.page
  let limit = 15

  Resource.paginate({}, { select: 'name description', sort: { name: 1 }, page: page, limit: limit }, function (err, resources) {
    res.json(resources)
  })

})

//route returns all resources paginated by letter
router.get('/resources/letter/:letter/:page', function (req, res) {

  let page = req.params.page
  let limit = 15
  let letter = req.params.letter.toLowerCase()
  let searchRegEx = `^${letter}`

  Resource.paginate({ "name": { $regex: searchRegEx, $options: "i" } }, { select: 'name description', sort: { name: 1 }, page: page, limit: limit }, function (err, resources) {
    res.json(resources)
  })

})

router.get('/resources/all', function (req, res) {

  Resource.find({}, 'name description')
    .then(function (resources) {
      res.json(resources)
    })
})


router.get('/resources/rtovalues', function (req, res) {
  ResourceRTOValue.find({})
    .then(function (rtoValues) {
      res.json(rtoValues)
    })
})

router.get('/resources/types', function (req, res) {
  ResourceType.find({})
    .then(function (resourceTypes) {
      res.json(resourceTypes)
    })
})

//route returns resources of given id
router.get('/resource/:resourceId', function (req, res) {

  Resource.findById(req.params.resourceId)
    .populate('dependencies')
    .then(function (resource) {
      //console.log(resource)
      res.json(resource)
    })

})

//route to update a resource
router.put('/resource', function (req, res) {
  console.log('updating resource...dependencies is:')
  console.log(req.body.dependencies)

  Resource.update({ _id: req.body._id }, {
    $set: {
      name: req.body.name,
      description: req.body.description,
      notes: req.body.notes,
      type: req.body.type,
      dependencies: req.body.dependencies,
      RTO: req.body.RTO,
      RPO: req.body.RPO,
      tags: req.body.tags,
      DRTestRequired: req.body.DRTestRequired
    }
  }, function (err, resource) {
    if (err) throw err
    res.json(resource)
  })

})


//Route adds a new resource
router.post('/resource', function (req, res) {
  let newResource = new Resource(req.body)
  newResource.save(function (err, newResource) {
    if (err) throw err
    console.log(`New Resource Added: ${newResource.name}`)
    res.json(newResource)
  })

})

//Route deletes a vender by sent Id
router.delete('/resource/:resourceId', function (req, res) {
  Resource.remove({ _id: req.params.resourceId }, function (err) {
    if (err) throw err
    res.json({ status: "ok", msg: "Resource deleted" })
  })
})

//////// contact ROUTES ///////////
//////////////////////////////////

// route to return all contact just names, id (GET http://localhost:9090/api/contactsids)
router.get('/contactids', function (req, res) {
  Contact.find({}, function (err, contactids) {
    res.json(contactids);
  });
});

//route to add a contacts
router.post('/contact', function (req, res) {
  //console.log(req.body)
  let newContact = new Contact(req.body)
  newContact.save(function (err, newContact) {
    if (err) throw err
    res.json(newContact)
  })
})

//////// Business Process ROUTES ///////////
//////////////////////////////////

// route to return all business processes (GET http://localhost:9090/api/bprocess)
router.get('/bprocess', function (req, res) {
  Bp.find({}, function (err, bprocess) {
    if (err) throw err
    res.json(bprocess)
  });
});


//route to add a business process
router.post('/bprocess', function (req, res) {
  //console.log(req.body)
  let newBProcess = new Bp(req.body)
  newBProcess.save(function (err, newBP) {
    if (err) throw err
    res.json(newBP)
  })
})

//route returns businessProcess of given id
router.get('/bprocess/:BpId', function (req, res) {

  Bp.findById(req.params.BpId)
    .populate('department requiredResources')
    .then(function (bp) {
      res.json(bp)
    })

})

//route to update a bp
router.put('/bprocess/:BpId', function (req, res) {
  console.log('req to update bp made')
  Bp.update({ _id: req.params.BpId }, {
    $set: {
      name: req.body.name,
      department: req.body.department,
      description: req.body.description,
      riskReputation: req.body.riskReputation,
      riskLegal: req.body.riskLegal,
      riskOperational: req.body.riskOperational,
      riskFinancial: req.body.riskFinancial,
      frequency: req.body.frequency,
      frequencyNotes: req.body.frequencyNotes,
      maxAllowedDowntime: req.body.maxAllowedDowntime,
      requiredResources: req.body.requiredResources
    }
  }, function (err, bp) {
    if (err) throw err
    res.json(bp)
  })
})



//Route deletes a Bp by sent Id
router.delete('/bprocess/:BpId', function (req, res) {
  Bp.remove({ _id: req.params.BpId }, function (err) {
    if (err) throw err
    res.json({ status: "ok", msg: "Business Process deleted" })
  })
})



//////// Blog ROUTES ///////////
//////////////////////////////////

// route to return all blogs
router.get('/blog/:page', function (req, res) {


  let page = req.params.page || 1
  let limit = 5

  Blog.paginate({}, { sort: { date: -1 }, page: page, limit: limit }, function (err, blogs) {
    res.json(blogs)
  })

})


//////// Bug ROUTES ///////////
//////////////////////////////////

// route to return all bugs
router.get('/bugs', function (req, res) {
  Bug.find({}, function (err, bugs) {
    if (err) throw err
    res.json(bugs)
  });
});

//route to add a bug
router.post('/bugs', function (req, res) {
  let newBug = new Bug(req.body)
  console.log(newBug)

  newBug.save(function (err, newBug) {
    if (err) throw err
    res.json(newBug)
  })

})

//export router
module.exports = router
