const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
//mongoose.Promise = require('bluebird')
const moment = require('moment')

let config = require('./config') //get our config
let routes = require('./routes') //get routes for dealing with users/personel

let port = process.env.PORT || 9090
mongoose.connect(config.database)
app.set('superSecret', config.secret) //secret variable

//allow cors on all routes

app.use(cors())


//use body PArser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended : false }))
app.use(bodyParser.json())

//use morgan to log requests to the console
app.use(morgan('dev'))

//Simple routes
app.get('/', function(req,res) {
  res.send(`Hello from Project Mayhem at: http://localhost:${port}`)
})

//apply the user routes to our application with the /api/personel prefix
app.use('/api/', routes)

// start the server ======
// =======================
app.listen(port)
console.log('Magic happens at http://localhost:' + port)
